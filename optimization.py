import numpy as np

def grad_estimator(fn):
    """
    Given a function as input, returns a *function* that gives the finite 
    differences approximation of the gradient. Only needed if the gradient 
    is not provided as input to ascent_optimize.
    """    
    def finite_difference_approx(x0, h=0.0000001, F=fn):
        return (F(x0+h) - F(x0))/h
    return finite_difference_approx

def backtrack_alpha(fn, x0, alpha, d, backtrack_perc, minstep):
    """
    Given input parameters, returns the next step in the descent iteration, performing
    backtracking as needed. Returns x_j+1 and alpha_j+1
    """
    while fn(x0+alpha*d) > fn(x0) and alpha>minstep:
        #print "Backtracking. Alpha={alpha}, Current={curr} | Next={next}".format(alpha=alpha, curr=fn(x0), next=fn(x0+alpha*d))
        alpha = alpha*backtrack_perc
    return x0+alpha*d, alpha
    
def ascent_iter(fn, x0, grad, alpha, eps, backtrack_perc, d_sign, minstep):
    """
    Runs a single iteration of an ascent optimization with backtracking
    """
    optimized=False
    del_f = grad(x0)
    del_f_mag = np.linalg.norm(del_f)
    if del_f_mag < eps:
        optimized = True
        return optimized, x0, alpha # early return point, ugly coding. Man I'm out of practice.
    else:
        d = d_sign*del_f/del_f_mag
        result, alpha = backtrack_alpha(fn, x0, alpha, d, backtrack_perc, minstep)
        print alpha, del_f_mag #, result
    return optimized, result, alpha

def ascent_optimize(fn, x0, grad=None, alpha=1, eps=.0000001, maxiters=1000, minstep=.0000000000001, backtrack_perc=.5, minimize=True, trace=False):
    """
    Implements an ascent optimization algorithm using backtracking.
    """
    if grad is None:
        grad = grad_estimator(fn)
    # This message passing feels unpythonic. I'm rusty.
    if minimize:
        d_sign=-1
    else:
        d_sign=1
    kargs = {'fn':fn, 'x0':x0, 'grad':grad, 'alpha':alpha, 'eps':eps, 'backtrack_perc':backtrack_perc, 'd_sign':d_sign, 'minstep':minstep}
    
    optimized=False
    iters=0
    history=[]
    while not optimized and iters < maxiters:
        iters+=1
        optimized, result, alpha = ascent_iter(**kargs)
        kargs['alpha']=alpha
        kargs['x0']=result
        if trace:
            history.append(result)
    if optimized:
        print "Optimization converged after {i} iterations.".format(i=iters)
    elif alpha < minstep: # should really handle these cases with different internally raised exceptions
        print "Alpha shrunk to below minimum threshhold. Returning result after {i} iterations.".format(i=iters)
    else:
        print "Optimization did not converge. Returning result after {i} iterations.".format(i=iters)
    if history:
        output = (result, history)
    else:
        output = result
    return output   
    
if __name__ == '__main__':
    # univariate tests
    def testfunc1(x):
        return x**2
    
    def testgrad(x):
        return 2*x
    
    ascent_optimize(fn=testfunc1, grad=testgrad, x0=1)
    ascent_optimize(fn=testfunc1, x0=1)
    # Univariate tests successful. Should run some mv tests.
    
    # Rosenbrock function lifted from:
    #   http://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html
    def rosen(x):
        """The Rosenbrock function"""
        return sum(100.0*(x[1:]-x[:-1]**2.0)**2.0 + (1-x[:-1])**2.0)

    def rosen_der(x):
        xm = x[1:-1]
        xm_m1 = x[:-2]
        xm_p1 = x[2:]
        der = np.zeros_like(x)
        der[1:-1] = 200*(xm-xm_m1**2) - 400*(xm_p1 - xm**2)*xm - 2*(1-xm)
        der[0] = -400*x[0]*(x[1]-x[0]**2) - 2*(1-x[0])
        der[-1] = 200*(x[-1]-x[-2]**2)
        return der

    p0 = np.array([1.3, 0.7, 0.8, 1.9, 1.2])
    test = ascent_optimize(fn=rosen, grad=rosen_der, x0=p0)