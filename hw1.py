# Problem 2
import energy_model
import optimization
import numpy as np

config0 = np.matrix([(0,0,0)
                    ,(1,0,0)
                    ,(.5,np.sqrt(3)/2,0)]) # this configuration is an optimum
config0b = np.matrix([(0,0,0)
                    ,(1,0,0)
                    ,(.8,np.sqrt(3)/2,0)]) # one coordinate changed, and we BARELY move.
config1 = np.matrix([(0,0,0),(.5,0,0),(.5,.5,0)])
config2 = np.matrix([(0,0,0),(1,0,0),(2,2,0)])
obs = [
    (0,0),(0,1),(0,2),
          (1,1),(1,2),
                (2,2)
 ]

# let's just try to optimize over that one position
obs_0c = obs[:]
obs_0c.append((1,0))
obs_0b = obs_0c[:]
obs_0b.append((2,1))
 
test0 = energy_model.EnergyModel(N=3, configuration=config0, observed=obs)      # optimal configuration
test0b = energy_model.EnergyModel(N=3, configuration=config0b, observed=obs_0b) # only 1 point unobserved
test0c = energy_model.EnergyModel(N=3, configuration=config0b, observed=obs_0c) # 2 points unobserved
test1 = energy_model.EnergyModel(N=3, configuration=config1, observed=obs)
test2 = energy_model.EnergyModel(N=3, configuration=config2, observed=obs)
res0 = test0.minimize_E()
res0b = test0b.minimize_E(maxiters=5000) 
res0c = test0c.minimize_E(maxiters=5000) # starts oscillating after 15 iterations 
res1 = test1.minimize_E()
res2 = test2.minimize_E()

energy_model.pdist(res0)
energy_model.pdist(res1)
energy_model.pdist(res2)
# Try playing with equilateral triangular configurations

####################################################33

# let's skip ahead a bit
# Problem 3

# set up our workspace.
with open('economic_data.txt','r') as f:
    data = f.readlines()

#econ = np.matrix([row.split() for row in data[35:]])

# actually, let's just read this straight into a matrix object.
# There might be an easier way to do this... oh well
econ = np.loadtxt(StringIO.StringIO(''.join(data[35:])))

# replace index column with ones and extract the response variable into
# a separate vector to build the design matrix
econ[:,0] = 1 
resp = np.matrix(econ[:,7]).T # weird that this coercion is necessary...
econ = econ[:,:7]

def RSS(B, X=econ, Y=resp):
    """
    Returns the sum of the squared residuals given a design matrix X,
    coefficients B and response vector Y. This is the objective function
    to be minimized in the optimization approach to regression.
    """
    return (Y - X*B).T * (Y - X*B)
    # coerce to float to handle array error from while loop in ascent alg

def del_RSS(B, X=econ, Y=resp):
    """
    Returns the gradient of the regression objective for a given weight vector
    """
    # I'm not positive if this is correct, but I think it's pretty close to the
    # correct form if it's not it exactly. The returned vector is at least the
    # shape. Might be missing a scalar, otherwise I think we're basically there.
    return -X.T*(Y-X*B)

# 3a)

# Ok, let's start by minimizing the reduced formula: A3 = b1 * A1 + b2
def obj_3a(B):    
    return RSS(B, X=econ[:,0:2], Y=np.matrix(econ[:,2]).T)

def grad_3a(B):
    return del_RSS(B, X=econ[:,0:2], Y=np.matrix(econ[:,2]).T)

b0=np.matrix([1, 10]).T
test0_3a = optimization.ascent_optimize(fn=obj_3a, grad=grad_3a, x0=b0)

# Let's sanity test with the finite differences approximation to the gradient
test0_3a = optimization.ascent_optimize(fn=obj_3a, x0=b0, maxiters=1000)

# Ok... maybe there's just something rwong with my gradient descent algorithm. Nuts. 