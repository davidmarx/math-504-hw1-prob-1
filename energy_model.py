"""
Models the van-der-Walls energy to find the configuration of unobserved
molecules in a system of N molecules were the positions of k<N molecules are
known.
"""

import numpy as np
import optimization
from scipy.spatial.distance import pdist, squareform

class EnergyModel(object):
    def __init__(self, N, observed, x0=None, configuration=None):
        """
        @param N:        The number of molecules in the model. Dimension is 
                         presumed to be 3 (x,y,z)
        @param observed: Tuples of the form (dim, j) to denote that the 
                         position of a particular molecule is known. For 
                         instance, to denote that the position of x2, y1, and
                         z1 and z2 should be fixed (zero indexing): 
                           observed = ((0,1)(1,2),(2,2),(2,1)
        @param x0:       A vector that gives the initial configuration of the 
                         model, including guesses at unobserved molecules.
        @param configuration: same thing as x0, just already cast as matrix.
        """
        self.N = N
        self.observed = observed
        if configuration is not None:
            self.configuration = configuration
        elif x0:
            self.cast_x0_to_config(x0)
        else:
            pass
        # else... initialize unobserved molecules in random configuration?
        # Really I should raise some exception here. Actually, it's not a 
        # a huge issue if we don't have a configuration to start with, we'll
        # get one later.
    
    # ready for testing    
    @property
    def r_ij(self):
        """
        Set to be calculated ONLY when configuration is updated.
        """
        return self._r_ij
    
    # Ready for test
    @property
    def configuration(self):
        return self._configuration
    
    # Ready for test
    @configuration.setter
    def configuration(self, value):
        self._configuration = value
        self._r_ij = squareform(pdist(self.configuration)) # recalculate this only when configuration is updated
    
    # ready for testing
    def U(self, r):
        """
        Calculates the Lennard-Jones potential. Input MUST be an ndarray and NOT a
        matrix. Function assumes elementwise broadcasting will work and, frankly,
        lazily does not do any type-checking or casting for speed.
        """
        return  np.nan_to_num( (1/r**12) - (2/r**6) )
    
    # ready for testing
    def E_grad_part_dU(self, dim, i, j):        
        """
        Returns the partial derivative of the Lennard-Jones potential.
        """
        r = self.r_ij
        x = self.configuration
        #print i,j,dim
        d = (x[i,dim] - x[j,dim]) / r[i,j]
        return np.nan_to_num( -12 * r**(-13) * d + 12 * r**(-7) * d )
        # I think we might need to set the diagonals to zero for numerical 
        # stability or something. Getting inf's and NaN's
    
    # Ready for testing
    def E_grad_part(self, dim, i):
        """
        Returns the partial derivative component of the gradient vector of the 
        energy function taken with respect to the given dimension.
        
        @param dim: one of 0, 1, 2 to denote the appropriate matrix dimension
                    (i.e. x, y or z)
        @param i:   the variable index, an integer in 0:N-1
        """
        grad = 0
        for j in range(self.N):
            grad += np.sum(self.E_grad_part_dU(dim, i, j))
        return grad
    
    # ready for test
    def cast_x0_to_config(self, x0):
        """
        Standardize how I cast x0 to a configuration matrix across functions
        """
        self.configuration = np.matrix(x0).reshape(self.N, 3)
    
    def cast_config_to_vector(self):
        """
        Standardizing how I cast molecular configuration matrices to vectors.
        Should be the inverse procedure of cast_x0_to_config. I could probably
        just tweak my optimization algorithm to handle matrices... oh well.
        """
        return np.matrix(self.configuration).reshape(3*self.N, 1)
    
    # ready for test
    def E_grad(self, x0=None, configuration=None):
        """
        Given an input configuration, molecules identified by their indices i and j, and
        and the dimension with respect to take the gradient, returns the partial gradient
        term taken with respect to r_ij.    
        Returns the gradient matrix with 0's at the positions of the "fixed"
        molecules to ensure that we don't modify their positions with each update.
        """                
        if configuration is not None:
            self.configuration = configuration
        else:
            # untested
            self.cast_x0_to_config(x0)
                         
        v = np.zeros((self.N, 3))   # reshape to column vector afterwards
        for d in range(3):
            for j in range(1,self.N):   # python is zero indexed, equivalent to "2 to m"
                if (j,d) in self.observed:
                    continue
                else:
                    v[j,d] = self.E_grad_part(d, j)
        return np.matrix(v.reshape(3*self.N, 1)) # return gradient as column vector
    
    # Ready for test
    def E(self, x0=None, configuration=None):
        """
        Returns the energy of a particular configuration of molecules.
        """
        if configuration is not None:
            self.configuration = configuration
        elif x0 is not None:
            self.cast_x0_to_config(x0)
        else:
            pass
            # should at least validate that self.configuration is populated
        r = self.r_ij
        #return np.sum( np.nan_to_num((1/r**12) - (2/r**6)) )
        return np.sum( self.U(r) )/2 
        # dividing by two to account for double counting from using distance 
        # matrix. Not really necessary, but whatever, it's correct. Should 
        # consider similarly rescaling gradient as well.
    
    def minimize_E(self, eps=.0000001, maxiters=100):
        return optimization.ascent_optimize(fn=self.E, grad=self.E_grad, x0=self.cast_config_to_vector(), eps=eps, maxiters=maxiters).reshape(self.N,3)